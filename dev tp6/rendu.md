# II. Chat room

- [II. Chat room](#ii-chat-room)
  - [1. Intro](#1-intro)
  - [2. Première version](#2-première-version)
  - [3. Client asynchrone](#3-client-asynchrone)
  - [4. Un chat fonctionnel](#4-un-chat-fonctionnel)
  - [5. Gérer des pseudos](#5-gérer-des-pseudos)
  - [6. Déconnexion](#6-déconnexion)

## 1. Intro



🌞 [`chat_server_ii_2.py`](https://gitlab.com/ynov18/tp3programmes/-/blob/main/tp6/chat_room/chat_server_ii_2.py?ref_type=heads)


🌞 [`chat_client_ii_2.py`](https://gitlab.com/ynov18/tp3programmes/-/blob/main/tp6/chat_room/chat_client_ii_2.py?ref_type=heads)



## 3. Client asynchrone



🌞 [`chat_client_ii_3.py`](https://gitlab.com/ynov18/tp3programmes/-/blob/main/tp6/chat_room/chat_client_ii_3.py?ref_type=heads)



🌞 [`chat_server_ii_3.py`](https://gitlab.com/ynov18/tp3programmes/-/blob/main/tp6/chat_room/chat_server_ii_3.py?ref_type=heads)


## 4. Un chat fonctionnel



🌞 [`chat_server_ii_4.py`](https://gitlab.com/ynov18/tp3programmes/-/blob/main/tp6/chat_room/chat_server_ii_4.py?ref_type=heads)



## 5. Gérer des pseudos


🌞 [`chat_client_ii_5.py`](https://gitlab.com/ynov18/tp3programmes/-/blob/main/tp6/chat_room/chat_client_ii_5.py?ref_type=heads)



🌞 [`chat_server_ii_5.py`](https://gitlab.com/ynov18/tp3programmes/-/blob/main/tp6/chat_room/chat_server_ii_5.py?ref_type=heads)



