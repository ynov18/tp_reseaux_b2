# TP1 : Maîtrise réseau du poste

Pour ce TP, on va utiliser **uniquement votre poste** (pas de VM, rien, quedal, quetchi).

Le but du TP : se remettre dans le bain tranquillement en manipulant pas mal de concepts qu'on a vu l'an dernier.

C'est un premier TP *chill*, qui vous(ré)apprend à maîtriser votre poste en ce qui concerne le réseau. Faites le seul ou avec votre mate préféré bien sûr, mais jouez le jeu, faites vos propres recherches.

La "difficulté" va crescendo au fil du TP, mais la solution tombe très vite avec une ptite recherche Google si vos connaissances de l'an dernier deviennent floues.

- [TP1 : Maîtrise réseau du poste](#tp1--maîtrise-réseau-du-poste)
- [I. Basics](#i-basics)
- [II. Go further](#ii-go-further)
- [III. Le requin](#iii-le-requin)

# I. Basics

> Tout est à faire en ligne de commande, sauf si précision contraire.

☀️ **Carte réseau WiFi**

```
PS C:\Users\Benedict> ipconfig /all
Adresse physique . . . . . . . . . . . : AC-ED-5C-5C-FC-C4  
Adresse IPv4. . . . . . . . . . . . . .: 192.168.43.126(préféré)
Masque de sous-réseau. . . . . . . . . : 255.255.255.0 /24
  - en notation CIDR, par exemple `/16`
  - ET en notation décimale, par exemple `255.255.0.0`
  ```

---

☀️ **Déso pas déso**

Pas besoin d'un terminal là, juste une feuille, ou votre tête, ou un tool qui calcule tout hihi. Déterminer...
192.168.43.126
- 192.168.43.0  (Adresse réseau)
- 192.168.43.255(adresse broadcast)
- 254

---

☀️ **Hostname**

```
PS C:\Users\Benedict> hostname
Laptop-Benedict
 ```

---

☀️ **Passerelle du réseau**

Déterminer...

-10.33.79.254
- AC-ED-5C-5C-FC-C4

---

☀️ **Serveur DHCP et DNS**

```
PS C:\Users\Benedict> ipconfig /all
Serveur DHCP . . . . . . . . . . . . . : 192.168.43.1
Serveurs DNS. . .  . . . . . . . . . . : 192.168.43.1

```

☀️ **Table de routage**

```
PS C:\Users\Benedict> route print
Destination réseau    Masque réseau  Adr. passerelle   Adr. interface Métrique
          0.0.0.0          0.0.0.0     10.33.79.254     10.33.76.221     35
```


# II. Go further


☀️ **Hosts ?**
```
PS C:\Windows\System32\drivers\etc> ping b2.hello.vous

Envoi d’une requête 'ping' sur b2.hello.vous [1.1.1.1] avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=41 ms TTL=50
Réponse de 1.1.1.1 : octets=32 temps=61 ms TTL=50
Réponse de 1.1.1.1 : octets=32 temps=46 ms TTL=50
Réponse de 1.1.1.1 : octets=32 temps=41 ms TTL=50
```

☀️ **Go mater une vidéo youtube et déterminer, pendant qu'elle tourne...**
```
PS C:\Windows\System32\drivers\etc> nslookup www.youtube.com
Serveur :   UnKnown
Address:  192.168.43.1

PS C:\Windows\System32\drivers\etc> netstat -an |findstr "ESTABLISHED"
TCP    192.168.43.126:54503   52.2.192.103:443       ESTABLISHED
```

☀️ **Requêtes DNS**
```
PS C:\Windows\System32\drivers\etc> nslookup www.ynov.com
Serveur :   UnKnown
Address:  192.168.43.1

PS C:\Windows\System32\drivers\etc> nslookup 174.43.238.89
Nom :    89.sub-174-43-238.myvzw.com
```

☀️ **Hop hop hop**
```
PS C:\Windows\System32\drivers\etc> tracert www.ynov.com

Détermination de l’itinéraire vers www.ynov.com [172.67.74.226]
avec un maximum de 30 sauts :

  1     6 ms     2 ms    10 ms  192.168.43.1
  2     *        *        *     Délai d’attente de la demande dépassé.
  3   252 ms    27 ms    28 ms  192.168.6.14
  4    56 ms    41 ms    58 ms  192.168.255.6
  5    57 ms    39 ms    36 ms  194.149.185.48
  6   298 ms    31 ms     *     194.149.173.42
  7    76 ms     *        *     99.83.88.183
  8    36 ms    35 ms    36 ms  prs-b3-link.ip.twelve99.net [62.115.46.68]
  9    47 ms    37 ms    54 ms  prs-bb1-link.ip.twelve99.net [62.115.118.58]
 10    60 ms   178 ms   126 ms  prs-b1-link.ip.twelve99.net [62.115.125.171]
 11     *       80 ms    53 ms  cloudflare-ic-375100.ip.twelve99-cust.net [80.239.194.103]
 12    60 ms    88 ms    72 ms  172.71.132.4
 13    72 ms    58 ms    76 ms  172.67.74.226
 soit 13 machines 
```
☀️ **IP publique**
```
PS C:\Windows\System32\drivers\etc> nslookup myip.opendns.com resolver1.opendns.com
>>
Serveur :   dns.opendns.com
Address:  208.67.222.222
```
☀️ **Scan réseau**
```
PS C:\Windows\System32\drivers\etc> arp -a

Interface : 192.168.43.126 --- 0x6
  Adresse Internet      Adresse physique      Type
  192.168.43.1          ae-5f-3e-79-e6-34     dynamique
  192.168.43.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 10.5.5.11 --- 0x13
  Adresse Internet      Adresse physique      Type
  10.5.5.255            ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  230.0.0.1             01-00-5e-00-00-01     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.105.1.1 --- 0x17
  Adresse Internet      Adresse physique      Type
  10.105.1.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
PS C:\Windows\System32\drivers\etc>
```

# III. Le requin

☀️ **Capture ARP**

- 📁 fichier `arp.pcap`
[capture TCP](./captures/arp.pcap.pcapng)

☀️ **Capture DNS**

- 📁 fichier `dns.pcap`
[capture TCP](./captures/dns.pcap.pcapng)

☀️ **Capture TCP**

- 📁 fichier `tcp.pcap`
[capture TCP](./captures/tcp.pcap.pcapng)


