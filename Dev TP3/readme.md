# TP3 DEV : Python et réseau

# I. Ping

🌞 [**Ping_Simple.py**](https://gitlab.com/ynov18/tp3programmes/-/blob/main/ping_simple.py)

🌞 [**Ping_argv.py**](https://gitlab.com/ynov18/tp3programmes/-/blob/main/ping_argv.py)

🌞 [**is_up.py**](https://gitlab.com/ynov18/tp3programmes/-/blob/main/is_up.py)

# II. DNS

🌞 [**look_up.py**](https://gitlab.com/ynov18/tp3programmes/-/blob/main/lookup.py)

# III. Get your IP

🌞 [**get_ip.py**](https://gitlab.com/ynov18/tp3programmes/-/blob/main/Get_IP.py)


# IV. Mix

🌞 [**Network.py**](https://gitlab.com/ynov18/tp3programmes/-/blob/main/ping_simple.py)

