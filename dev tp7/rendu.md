# TP7 DEV : Websockets et databases


## I. Websockets



### 1. First steps



🌞 **[`ws_i_1_server.py`](https://gitlab.com/ynov18/tp3programmes/-/blob/main/tp7/ws_i_1_servers.py?ref_type=heads) et [`ws_i_1_client.py`](https://gitlab.com/ynov18/tp3programmes/-/blob/main/tp7/ws_i_1_client.py?ref_type=heads)**




### 2. Client JS


🌞 **[`ws_i_2_client.js`](https://gitlab.com/ynov18/tp3programmes/-/blob/main/tp7/ws_i_2_client.html?ref_type=heads)**


### 3. Chatroom magueule

🌞 **[`ws_i_3_server.py`](https://gitlab.com/ynov18/tp3programmes/-/blob/main/tp7/chat_server_ii_5.py?ref_type=heads) et [`ws_i_3_client.{py,js}`](https://gitlab.com/ynov18/tp3programmes/-/blob/main/tp7/chat_client_ii_5.py?ref_type=heads)**



